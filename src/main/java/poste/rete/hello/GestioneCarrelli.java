package poste.rete.hello;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("carrelliman")
public class GestioneCarrelli {
	
	private static HashMap<String,Carrello> carrelli = new HashMap<>();
	
	
	public GestioneCarrelli() {
		Carrello c = new Carrello();
		c.setId("prova");
		c.addItem("articolo1");
		c.addItem("Articolo2");
		
		Carrello b = new Carrello();
		b.setId("prova2");
		b.addItem("hello");
		
		Carrello f = new Carrello();
		f.setId("prova3");
		
		carrelli.put(b.getId(), b);
		carrelli.put(c.getId(), c);
		carrelli.put(f.getId(), f);
	}

	
	@GetMapping
	public HashMap<String, Carrello> getAll(){
		return carrelli;
	}
	
	
	@GetMapping("{id}")
	public Carrello get(@PathVariable("id") String id) {
		return carrelli.get(id);
	}
	
	
	@PostMapping
	public ResponseEntity<Carrello> creaCarrello(@RequestBody Carrello c) {
		ResponseEntity<Carrello> res;
		if(!carrelli.containsKey(c.getId())) {
			res = new ResponseEntity<Carrello>(c, HttpStatus.CREATED);
			carrelli.put(c.getId(), c);
		}else {
			res = new ResponseEntity<Carrello>(c, HttpStatus.CONFLICT);
		}
		return res;
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<String> rmCarrello(@PathVariable("id") String id ) {
		ResponseEntity<String> res;
		if(!carrelli.containsKey(id)) {
			res = new ResponseEntity<String>(id, HttpStatus.NOT_FOUND);
		}else {
			res = new ResponseEntity<String>(id, HttpStatus.FOUND);
			carrelli.remove(id);
		}
		return res;
	}
	
	@PutMapping("{id}")
	public ResponseEntity<String> updateCarrello(@PathVariable("id") String id, @RequestBody Carrello c ) {
		ResponseEntity<String> res;
		if(carrelli.containsKey(id)) {
			res = new ResponseEntity<String>(id, HttpStatus.FOUND);
			carrelli.put(id, c);
		}else {
			res = new ResponseEntity<String>(id, HttpStatus.NOT_FOUND);
		}
		return res;
	}

}
