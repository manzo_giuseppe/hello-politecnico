package poste.rete.hello;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Carrello implements Serializable{
	
	private String id;
	private ArrayList<String> items = new ArrayList<>();
	
	public Carrello() {}
	
	public Carrello(String id, String[] i) {
		setId(id);
		items.addAll(Arrays.asList(i));
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<String> getItems() {
		return items;
	}
	
	public void addItem(String item) {
		items.add(item);
	}
	
	public boolean removeItem(String item) {
		return items.remove(item);
	}

}
